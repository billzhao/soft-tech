### Bio

8年互联网从业经历，具备优秀的产品sense和丰富的互联网项目研发经验。

途牛技术专家委员会成员；途牛研发高级经理；组织变革者；敏捷教练；

主持途牛运营项目（途牛年度项目提名奖）；
主持途牛竞拍项目（GP，保持年度20%增长）；
主持途牛玩法项目（覆盖全球目的地，覆盖路线20万以上）；

目前在SaaS领域探索的创业者。

-- 4+ 研发团队管理经验
-- 7+ LNMP开发经验
-- 7+ B/S项目经验
-- 技术领域：ORM、框架、TDD、设计模式、NoSql
-- 业务领域：BPM、SCM、B2B、OFBiz
-- GitHub：https://github.com/ftwbzhao
-- GitBook：https://www.gitbook.com/@ftwbzhao
