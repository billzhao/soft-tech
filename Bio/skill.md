## 能力卡片

### 组织变革

- 善于组织变革
- 组织流程改进
- 组织氛围提升

### 敏捷教练

- SM
- 实体看板
- 瀑布式开发
- 精细化研发管理

### 技术文章

- [PHP架构]( http://mp.weixin.qq.com/s?__biz=MzAwOTE0ODEwMQ==&mid=403288937&idx=1&sn=1ac81f1c8c0465e32cba9dd6f489fd6a&scene=21)
- [权限设计]( https://mp.weixin.qq.com/s?__biz=MzAwOTE0ODEwMQ==&mid=2650686231&idx=1&sn=d669f7afdb16af67420f5be24b936fc1)
- [EAV模型](http://mp.weixin.qq.com/s?__biz=MzAwOTE0ODEwMQ==&mid=2650686040&idx=1&sn=da67db52b2c188f7707a68a3df39b240)
- [CMS](http://mp.weixin.qq.com/s?__biz=MzAwOTE0ODEwMQ==&mid=2650686076&idx=1&sn=53df3a7ab61766df64fe302a9d296a3c)
- [Redis](https://mp.weixin.qq.com/s?__biz=MzAwOTE0ODEwMQ==&mid=403208856&idx=1&sn=1b345b8d3bb9d3f54be6125abb600edb)

### 业务架构

![ECO](http://7xp19f.com1.z0.glb.clouddn.com/op-eco.png)
![OP](http://7xp19f.com1.z0.glb.clouddn.com/op.png)
![CMS](http://7xp19f.com1.z0.glb.clouddn.com/cms.png)
![CMS](http://7xp19f.com1.z0.glb.clouddn.com/cms-module-parser.png)
