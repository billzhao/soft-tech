## 微信

[Redis](http://git.tuniu.org/zhaoyang2/soft-tech/blob/master/wx/redis.md)：无线中心运营研发Redis酷实践。

> 当你手上有一把锤子的时候，看所有的东西都是钉子。

[CMS](http://git.tuniu.org/zhaoyang2/soft-tech/blob/master/wx/cms.md)：运营神器之高效的CMS

> 用技术带动生产。

[RBZ](http://git.tuniu.org/zhaoyang2/soft-tech/blob/master/wx/rbz.md)：基于EAV模型的运营系统架构实践

> 人人都是程序员。

[PHP](http://git.tuniu.org/zhaoyang2/soft-tech/blob/master/wx/php.md)：大话权限中心的PHP架构之道

> 享受编程和技术所带来的快乐。

[权限架构](http://git.tuniu.org/zhaoyang2/soft-tech/blob/master/wx/rbac.md)：途牛无线权限系统的架构设计与实践

> 享受编程和技术所带来的快乐。

[CMS架构](http://git.tuniu.org/zhaoyang2/soft-tech/blob/master/wx/arch.md)：途牛CMS系统的架构设计与实践

> 享受编程和技术所带来的快乐。
