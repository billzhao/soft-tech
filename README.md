### 小荷才露尖尖角

Inspire By 鸟哥的这次采访 **[《技术人员如何保持进步》](http://www.infoq.com/cn/articles/laruence-lianjia)**。

于是乎，决定不在 _”酒香不怕巷子深“_ ，开始尝试去写文（zhuang）章（bi）啦。。。

:dancers:

:dancers:

:dancers:

### ~~勿忘~~初心

> 分享&传阅知识，理解技术，合理运用，造福人类。

> 享受编程和技术所带来的快乐。 [拜山头](http://coolshell.cn/)

### 脉络

- [微信](https://ftwbzhao.gitbooks.io/soft-tech/content/wx/index.html)

### 工具

- ~~[AsciiDoc](http://asciidoctor.org/)~~
-  :rocket:[Markdown](http://daringfireball.net/projects/markdown/syntax)

### 联系方式

- email：b.zhao1@gmail.com
- github：[ftwbzhao](https://github.com/ftwbzhao)
